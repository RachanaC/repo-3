package com.pack.crudoperation.controller;

import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pack.crudoperation.model.PetCustomerDetails;
import com.pack.crudoperation.model.PetDetails;
import com.pack.crudoperation.service.ServiceClass;

@Controller
public class ControllerClass {

	@Autowired
	private ServiceClass serviceClass;

	@GetMapping(value = "/")
	public String homePage(ModelMap model) {
		model.addAttribute("petCustomerDetails", new PetCustomerDetails());
		return "homePage";
	}

	@GetMapping(value = "/register")
	public String customerRegistration(ModelMap model) {
		model.addAttribute("petCustomerDetails", new PetCustomerDetails());
		return "customerRegistration";
	}

	@GetMapping(value = "/petHome")
	public String petHome(ModelMap model) {
		model.addAttribute("petDetails", new PetDetails());

		return "petHome";
	}

	@PostMapping(value = "/addpets")
	public String addpetDetails(ModelMap model) {
		// model.addAttribute("petDetails", new PetDetails());

		List<PetDetails> list = serviceClass.getPetDetailsById();
		model.addAttribute("petList", list);

		return "redirect:/petHome";

	}

	@GetMapping("/petRegistration")
	public ModelAndView petRegistration(ModelAndView model) {
		model.addObject("petDetails", new PetDetails());
		model.setViewName("petRegistration");
		return model;
	}

	@GetMapping(value = "/login")
	public String loginPage(ModelMap model) {
		model.addAttribute("petCustomerDetails", new PetCustomerDetails());
		return "loginPage";
	}

	@GetMapping(value = "/logout")
	public String logout(ModelMap model) {
		model.addAttribute("petCustomerDetails", new PetCustomerDetails());
		return "loginPage";
	}

	@PostMapping(value = "/save")
	public String savePetCustomerDetails(Model model,
			@Valid @ModelAttribute("petCustomerDetails") PetCustomerDetails pcd, BindingResult result) {
		String resultForm = "";
		if (result.hasErrors()) {
			resultForm = "customerRegistration";
		} else {
			Random r = new Random();
			int id = r.nextInt(999999) + 100000;
			pcd.setId(id);
			serviceClass.savePetCustomerDetails(pcd);
			List<PetCustomerDetails> list = serviceClass.getPetCustomerDetailsById();
			model.addAttribute("list", list);

			resultForm = "redirect:/viewCustomerDetails";
		}
		return resultForm;
	}

	@GetMapping(value = "/delete")
	public String deleteAllPetCustomerDetails() {
		serviceClass.deleteAllPetCustomerDetails();
		return "redirect:/register";
	}

	@GetMapping(value = "/editCustomerDetails/{petcusid}")
	public String editStudent(@PathVariable("petcusid") Integer PetCustomerDetailsId, ModelMap model) {
		PetCustomerDetails petCustomerDetails = serviceClass.getPetCustomerDetailsById(PetCustomerDetailsId);
		model.addAttribute("editCustomerDetails", petCustomerDetails);
		return "editCustomerDetails";

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updatePetCustomerDetails(
			@Valid @ModelAttribute("editCustomerDetails") PetCustomerDetails petCustomerDetails, BindingResult result) {
		String editform = "";
		if (result.hasErrors()) {
			editform = "editCustomerDetails";
		} else {
			serviceClass.updatePetCustomerDetails(petCustomerDetails);
			editform = "redirect:/viewPetCustomerDetails";
		}
		return editform;
	}

	@GetMapping(value = "/delete)PetCustomerDetails/{petcusid}")
	public String deleteStudent(@PathVariable("petcusid") Integer pid) {
		serviceClass.deletePetCustomerDetails(pid);
		return "redirect:/viewPetCustomerDetails";
	}

}
