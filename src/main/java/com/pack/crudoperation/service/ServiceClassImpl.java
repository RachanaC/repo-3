package com.pack.crudoperation.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.crudoperation.dao.PetRepositoryClass;
import com.pack.crudoperation.dao.RepositoryClass;
import com.pack.crudoperation.model.PetCustomerDetails;
import com.pack.crudoperation.model.PetDetails;

@Service
public class ServiceClassImpl implements ServiceClass {

	@Autowired
	private RepositoryClass repositoryClass;
	

	@Override
	public void savePetCustomerDetails(PetCustomerDetails petCustomerDetails) {
		System.out.println("Inside savepet and petCustomerDetails is :: "+petCustomerDetails.getFirstName());
		repositoryClass.save(petCustomerDetails);

	}

	@Override
	public void deleteAllPetCustomerDetails() {

		repositoryClass.deleteAll();
	}

	@Override
	public PetCustomerDetails getPetCustomerDetailsById(int petCustomerDetailsId) {

		Optional<PetCustomerDetails> optional = repositoryClass.findById(petCustomerDetailsId);
		PetCustomerDetails pcd = null;
		if (optional.isPresent()) {
			pcd = optional.get();
			System.out.println(optional.get());

		} else {
			System.out.printf("No student found with id %d%n", petCustomerDetailsId);

		}
		return pcd;
	}

	@Override
	public void updatePetCustomerDetails(PetCustomerDetails pcd) {
		repositoryClass.save(pcd);

	}

	@Override
	public void deletePetCustomerDetails(int petCustomerDetailsId) {

		repositoryClass.deleteById(petCustomerDetailsId);
	}

	@Override
	public List<PetCustomerDetails> getPetCustomerDetailsById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PetDetails> getPetDetailsById() {
		// TODO Auto-generated method stub
		return null;
	}

/*	@Override
	public void addpetDetails(PetDetails PetDetails) {
		// TODO Auto-generated method stub
		System.out.println("Inside savepet and petCustomerDetails is :: "+PetDetails.getPetname());
		petrepositoryclass.addpets(PetDetails);
		
	}*/

	//@Override
	//public List<PetDetails> getPetDetails() {
		// TODO Auto-generated method stub
		//return null;
	//}

	


	

}
