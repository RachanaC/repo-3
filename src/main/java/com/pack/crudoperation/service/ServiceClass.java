package com.pack.crudoperation.service;




import java.util.List;

import org.springframework.stereotype.Service;

import com.pack.crudoperation.model.PetCustomerDetails;
import com.pack.crudoperation.model.PetDetails;

@Service
public interface ServiceClass {

	public void savePetCustomerDetails(PetCustomerDetails petCustomerDetails);

	public  void deleteAllPetCustomerDetails();

	public   PetCustomerDetails getPetCustomerDetailsById(int petCustomerDetailsId);

	public void updatePetCustomerDetails(PetCustomerDetails pcd) ;
	
	public  void deletePetCustomerDetails(int petCustomerDetailsId) ;

	public List<PetCustomerDetails> getPetCustomerDetailsById();

	public List<PetDetails> getPetDetailsById();

	

	//public List<PetDetails> getPetDetails();

	//public void savePetDetail(PetDetails petDetail);

	//public List<petDetails> getPetDetails();

	
}