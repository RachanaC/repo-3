package com.pack.crudoperation.dao;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import com.pack.crudoperation.model.PetDetails;

@EnableJpaRepositories
@Repository
public interface PetRepositoryClass extends JpaRepository<PetDetails,Integer> {


}
	
	

