package com.pack.crudoperation.dao;



import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pack.crudoperation.model.PetCustomerDetails;
import com.pack.crudoperation.model.PetDetails;

@EnableJpaRepositories
@Repository
public interface RepositoryClass extends JpaRepository<PetCustomerDetails,Integer> {




}

